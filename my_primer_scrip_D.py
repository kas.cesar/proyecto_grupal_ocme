import os
import numpy as np
import pandas as pd
import json
from collections import defaultdict


class Jsons2Dict:
    def __init__(self, path):
        import pdb; pdb.set_trace()
        self.path = path
        self.jsons = self.collect_jsons()
        self.uniques_keys = self.collect_keys()
        self.data = self.merge_data()
        
    def collect_jsons(self):
        jsons = []
        files = os.listdir(self.path)
        for file in files:
            if not file.endswith('.json'):
                continue
            jsons.append(file)
        return jsons
    
    def collect_keys(self):
        keys = []
        for file in self.jsons:
            filetest = f'{self.path}/{file}'
            openfile = open(filetest, 'r')
            data = json.load(openfile)
            openfile.close()

            ### trabajos aqui

            for key in data.keys():
                keys.append(key)
    
        return set(keys)
    
    def merge_data(self):
        
        DATA = defaultdict(list)
        for file in self.jsons:
            filetest = f'{self.path}/{file}'
            openfile = open(filetest, 'r')
            data = json.load(openfile)
            openfile.close()

            ### trabajos aqui

            for key in self.uniques_keys:
                ret = data.get(key, None)
                DATA[key].append(ret)
    
        return DATA
    
    def show_columns(self):
        for key, val in self.data.items():
            print(f'{key:-<25}> {len(val)}')

path = '/home/cesar/Documentos/curso_ml/prueba_2/DATA'
jsons2dict = Jsons2Dict(path)